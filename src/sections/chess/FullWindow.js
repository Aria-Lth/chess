import React, { Component } from "react";

import UpgradeWindow from "./UpgradeWindow";
import SettingsWindow from "./SettingsWindow";
import Board from "./Board";
import Toolbar from "./Toolbar";
import History from "./History";

import setUpBoard from "./fxLibrary/setUpBoard";
import moves from "./fxLibrary/moves";
import deepCopy from "./fxLibrary/deepCopy";
import upgradePawn from "./fxLibrary/upgradePawn";

import { setsOfPieces, colors } from "./chessRessources";

export default class FullWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
    };
  }

  componentDidMount() {
    this.setState(() => {
      const chessBoard = [];
      for (let y = 7; y >= 0; y--) {
        chessBoard[y] = [];
        for (let x = 0; x < 8; x++) {
          chessBoard[y][x] = {
            color: y % 2 === x % 2 ? "white" : "black",
            x,
            y,
            status: "",
            owned: false,
          };
        }
      }
      setUpBoard(chessBoard);
      /* *** Initial state *** */
      return {
        isLoaded: true,
        chessBoard,
        turn: "white",
        selection: {},
        history: [],
        setsOfPieces: setsOfPieces,
        pieces: setsOfPieces[0],
        colors,
        selectedColor: colors[0],
        save: [],
        progress: 0,
        displayHistory: false,
        windowToDisplay: "board",
      };
    });
  }

  resetChessboard = () =>
    this.setState({
      chessBoard: setUpBoard(this.state.chessBoard),
      turn: "white",
      selection: {},
      history: [],
    });

  selectPiece = (square) => {
    if (!this.state.pause) {
      if (square.owned === this.state.turn) {
        this.setState((prevState) => {
          if (
            prevState.selection.y === square.y &&
            prevState.selection.x === square.x
          ) {
            return { selection: {} };
          } else {
            return { selection: { ...square } };
          }
        });
      } else if (Object.keys(this.state.selection).length) {
        const move = moves(
          this.state.selection,
          square,
          this.state.chessBoard,
          this.state.history
        );
        if (move) {
          this.setState(
            (prevState) => ({
              chessBoard: move.updatedBoard,
              selection: {},
              history: move.updatedHistory,
              pause: move.action.includes("upgrade") ? "upgrade" : false,
              save: [
                ...prevState.save,
                {
                  history: deepCopy(prevState.history, "history"),
                  chessBoard: prevState.chessBoard,
                  turn: prevState.turn,
                },
              ],
              progress: ++prevState.progress,
            }),
            () => !this.state.pause && this.switchTurn()
          );
        }
      }
    }
  };

  switchTurn = () =>
    this.setState((prevState) => ({
      turn:
        prevState.turn === "white"
          ? "black"
          : prevState.turn === "black" && "white",
    }));

  handleUpgrade = (choice) =>
    this.setState(
      (prevState) => {
        const upgrade = upgradePawn(
          prevState.chessBoard,
          choice,
          prevState.history
        );
        return {
          pause: false,
          chessBoard: upgrade.updatedBoard,
          history: upgrade.updatedHistory,
        };
      },
      () => this.switchTurn()
    );

  manageSettingsWindow = () =>
    this.setState((prevState) =>
      !prevState.pause
        ? {
            pause: "settings",
            windowToDisplay: "settings"
          }
        : prevState.pause === "settings"
        ? {
            pause: false,
            windowToDisplay: "board"
          }
        : {}
    );

  handleSetSelection = (e) =>
    this.setState({
      pieces: setsOfPieces[e.target.value],
    });

  handleColorSelection = (e) =>
    this.setState({
      selectedColor: colors[e.target.value],
    });

  revert = () =>
    !this.state.pause &&
    this.state.history.length &&
    this.setState(
      (prevState) => ({
        chessBoard: deepCopy(
          prevState.save[prevState.progress - 1].chessBoard,
          "board"
        ),
        history: prevState.save[prevState.progress - 1].history,
        progress: --prevState.progress,
        selection: {},
        save: prevState.save.slice(0, prevState.save.length - 1),
      }),
      () => this.switchTurn()
    );

  displayHistoryWindow = () =>
    this.setState((prevState) => ({
      displayHistory: !prevState.displayHistory,
    }));

  render() {
    if (!this.state.isLoaded) {
      return <p>Loading...</p>;
    }
    const { button, mainColor, secColor } = this.props;
    return (
      <>
        <UpgradeWindow
          status={this.state.pause}
          handleUpgrade={this.handleUpgrade}
          button={button}
        />
        <div className="mainSpace">
          <SettingsWindow
            windowToDisplay={this.state.windowToDisplay}
            handleSetSelection={this.handleSetSelection}
            setsOfPieces={this.state.setsOfPieces}
            pieces={this.state.pieces}
            handleColorSelection={this.handleColorSelection}
            colors={this.state.colors}
            resetChessboard={this.resetChessboard}
            manageSettingsWindow={this.manageSettingsWindow}
            historyLength={this.state.history.length}
            button={button}
          />
          <Board
            secColor={secColor}
            windowToDisplay={this.state.windowToDisplay}
            chessBoard={this.state.chessBoard}
            selectedColor={this.state.selectedColor}
            selection={this.state.selection}
            selectPiece={this.selectPiece}
            pieces={this.state.pieces}
          />
        </div>
        <Toolbar
          secColor={secColor}
          manageSettingsWindow={this.manageSettingsWindow}
          mainColor={mainColor}
          displayHistoryWindow={this.displayHistoryWindow}
          historyLength={this.state.history.length}
          revert={this.revert}
        />
        <History
          history={this.state.history}
          displayHistory={this.state.displayHistory}
          mainColor={this.props.mainColor}
          secColor={this.props.secColor}
        />
      </>
    );
  }
}
