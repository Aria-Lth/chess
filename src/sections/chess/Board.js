import React from "react";

const Board = ({
  secColor,
  windowToDisplay,
  chessBoard,
  selectedColor,
  selection,
  selectPiece,
  pieces,
}) => (
  <div
    className="fullBoard center"
    style={{
      boxShadow: `0px 0px 20px 3px ${secColor}`,
      opacity: windowToDisplay === "board" ? "1" : "0",
      transform: windowToDisplay === "board" ? "scale(1)" : "scale(0)",
      transitionProperty: "transform, box-shadow, opacity",
      transition: "transform 0s, box-shadow 300ms, opacity 300ms",
      transitionDelay:
        windowToDisplay === "board" ? "0s, 300ms, 300ms" : "300ms, 0s, 0s"
    }}
  >
    {chessBoard.map((row, iRow) => (
      <div className="row" key={iRow}>
        {row.map((square, iSquare) => (
          <div
            className="square"
            key={iSquare}
            style={{
              backgroundColor: selectedColor[square.color],
              boxShadow:
                parseInt(selection.y) === square.y &&
                parseInt(selection.x) === square.x
                  ? "inset 0 0 0 5px lightBlue"
                  : "",
              display: "flex",
              justifyContent: "center",
            }}
            onClick={() => selectPiece(square)}
          >
            <img
              src={pieces[`${square.owned[0]}${square.status}`]}
              alt={square.status}
              style={{
                maxWidth: "100%",
                maxHeight: "100%",
                backgroundColor: selectedColor[square.color],
              }}
            />
          </div>
        ))}
      </div>
    ))}
  </div>
);

export default Board;
