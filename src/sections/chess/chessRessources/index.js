import classicFullSet from "./classicFull";
import classicIconsSet from "./classicIcons";
import chromeIconsSet from "./chromeIcons";
import colors from "./colors";
import optionIcons from "./optionIcons";

const setsOfPieces = [classicFullSet, chromeIconsSet, classicIconsSet];

export { setsOfPieces, colors, optionIcons };
/*
  Convert svg with svgr by cli (eye.svg is example) :
  npx @svgr/cli --icon --replace-attr-values "#000000=currentColor" eye.svg
*/