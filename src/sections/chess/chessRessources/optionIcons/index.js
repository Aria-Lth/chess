import HistorySvg from "./HistorySvg";
import RevertSvg from "./RevertSvg";
import SettingsSvg from "./SettingsSvg";

export default {
  "HistorySvg": HistorySvg,
  "RevertSvg": RevertSvg,
  "SettingsSvg": SettingsSvg
};
