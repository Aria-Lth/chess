export default (data, name) => {
  switch (true) {
    case name === "board":
      const newBoard = [];
      data.forEach((row, iRow) => {
        newBoard[iRow] = [];
        row.forEach((square, iSquare) => {
          newBoard[iRow][iSquare] = { ...square };
        });
      });
      return newBoard;
    case name === "history":
      return [...data];
    default:
      console.error("Error on deep copy: unrecognised name");
      return data;
  }
};
