export default (board, choice, history) => {
  const updatedBoard = [...board];
  const updatedHistory = [...history];
  const response = {};
  let indexOfPawn = "";
  updatedBoard.find((row, iRow) => {
    if (iRow === 0 || iRow === 7) {
      row.find((square, iColumn) => {
        square.status === "P" && (indexOfPawn = `${iRow}${iColumn}`);
        return square.status === "P";
      });
    }
    return indexOfPawn;
  });
  updatedBoard[indexOfPawn[0]][indexOfPawn[1]].status = choice;

  updatedHistory[updatedHistory.length - 1] += `=${choice}`;

  response.updatedBoard = updatedBoard;
  response.updatedHistory = updatedHistory;
  return response;
};
