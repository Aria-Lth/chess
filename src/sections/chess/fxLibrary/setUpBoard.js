export default board =>
  board.map((row, iRow) => {
    switch (true) {
      case iRow === 0:
        row[0].status = "R";
        row[1].status = "H";
        row[2].status = "B";
        row[3].status = "Q";
        row[4].status = "K";
        row[5].status = "B";
        row[6].status = "H";
        row[7].status = "R";
        row.map(square => (square.owned = "black"));
        return row;
      case iRow === 1:
        return row.map(square => {
          square.status = "P";
          square.owned = "black";
          return square;
        });
      case iRow === 6:
        return row.map(square => {
          square.status = "P";
          square.owned = "white";
          return square;
        });
      case iRow === 7:
        row[0].status = "R";
        row[1].status = "H";
        row[2].status = "B";
        row[3].status = "Q";
        row[4].status = "K";
        row[5].status = "B";
        row[6].status = "H";
        row[7].status = "R";
        row.map(square => (square.owned = "white"));
        return row;
      default:
        return row.map(square => ({ ...square, status: "", owned: false }));
    }
  });
