export default (start, end) => {
  if (start.x === end.x) {
    if (start.y > end.y) {
      return "up";
    } else if (start.y < end.y) {
      return "down";
    }
  } else if (start.y === end.y) {
    if (start.x > end.x) {
      return "left";
    } else if (start.x < end.x) {
      return "right";
    }
  } else if (
    start.x > end.x &&
    start.y > end.y &&
    start.x - end.x === start.y - end.y
  ) {
    return "upleft";
  } else if (
    start.x < end.x &&
    start.y > end.y &&
    end.x - start.x === start.y - end.y
  ) {
    return "upright";
  } else if (
    start.x > end.x &&
    start.y < end.y &&
    start.x - end.x === end.y - start.y
  ) {
    return "downleft";
  } else if (
    start.x < end.x &&
    start.y < end.y &&
    end.x - start.x === end.y - start.y
  ) {
    return "downright";
  }
  return false;
};
