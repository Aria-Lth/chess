import { convertHistoryToObject } from "../convertHistory";

const checkPawnPath = (board, start, end) => {
  if (start.x === end.x) {
    if (start.owned === "white") {
      if (
        (end.y === start.y - 1 ||
          (start.y === 6 &&
            end.y === start.y - 2 &&
            !board[end.y + 1][end.x].status)) &&
        !board[end.y][end.x].status
      ) {
        return true;
      }
    } else if (start.owned === "black") {
      if (
        (end.y === start.y + 1 ||
          (start.y === 1 &&
            end.y === start.y + 2 &&
            !board[end.y - 1][end.x].status)) &&
        !board[end.y][end.x].status
      ) {
        return true;
      }
    }
  }
  return false;
};

const checkPawnTake = (start, end) => {
  if (start.owned === "white") {
    if (
      end.owned === "black" &&
      end.y === start.y - 1 &&
      (end.x === start.x - 1 || end.x === start.x + 1)
    ) {
      return true;
    }
  } else if (start.owned === "black") {
    if (
      end.owned === "white" &&
      end.y === start.y + 1 &&
      (end.x === start.x - 1 || end.x === start.x + 1)
    ) {
      return true;
    }
  }
  return false;
};

const checkEnPassant = (board, start, end, history) => {
  if (history.length) {
    const prevMove = convertHistoryToObject(history[history.length - 1]);
    if (start.y === 3 && end.y === 2) {
      if (start.x === end.x + 1) {
        if (
          board[start.y][start.x - 1].status === "P" &&
          board[start.y][start.x - 1].owned === "black" &&
          start.x === prevMove.startX + 1 &&
          start.y === prevMove.startY + 2 &&
          start.x === prevMove.endX + 1 &&
          start.y === prevMove.endY
        ) {
          return true;
        }
      } else if (start.x === end.x - 1) {
        if (
          board[start.y][start.x + 1].status === "P" &&
          board[start.y][start.x + 1].owned === "black" &&
          start.x === prevMove.startX - 1 &&
          start.y === prevMove.startY + 2 &&
          start.x === prevMove.endX - 1 &&
          start.y === prevMove.endY
        ) {
          return true;
        }
      }
    } else if (start.y === 4 && end.y === 5) {
      if (start.x === end.x + 1) {
        if (
          board[start.y][start.x - 1].status === "P" &&
          board[start.y][start.x - 1].owned === "white" &&
          start.x === prevMove.startX + 1 &&
          start.y === prevMove.startY - 2 &&
          start.x === prevMove.endX + 1 &&
          start.y === prevMove.endY
        ) {
          return true;
        }
      } else if (start.x === end.x - 1) {
        if (
          board[start.y][start.x + 1].status === "P" &&
          board[start.y][start.x + 1].owned === "white" &&
          start.x === prevMove.startX - 1 &&
          start.y === prevMove.startY - 2 &&
          start.x === prevMove.endX - 1 &&
          start.y === prevMove.endY
        ) {
          return true;
        }
      }
    }
  }
  return false;
};

export { checkPawnPath, checkPawnTake, checkEnPassant };
